import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tech_news/widgets/news_card.dart';

class Saved extends StatefulWidget {
  @override
  _SavedState createState() => _SavedState();
}

class _SavedState extends State<Saved> {
  var news = [];
  var loading = true;

  void getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var list = prefs.getStringList('news') ?? [];
    var newsArray = [];
    for (var i = 0; i < list.length; i++) {
      newsArray.add(json.decode(list[i]));
    }
    setState(() {
      news = newsArray;
      loading = false;
    });
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Material(
      color: Colors.transparent,
      child: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(color: Color(0xff444444)),
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            loading
                ? Container(
                    color: Color(0xff444444),
                    width: size.width,
                    height: size.height,
                    child: Center(
                      child: Container(
                          width: 150,
                          height: 150,
                          child:
                              Lottie.asset('lib/assets/lottie/loading.json')),
                    ),
                  )
                : news.length == 0
                    ? Center(
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                  width: 200,
                                  height: 200,
                                  child: Lottie.asset(
                                      'lib/assets/lottie/empty.json')),
                            )))
                    : Transform(
                        transform: Matrix4.translationValues(0.0, -35.0, 0.0),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              SizedBox(
                                height: 105,
                              ),
                              Transform(
                                transform:
                                    Matrix4.translationValues(0.0, -35.0, 0.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Container(
                                      width: size.width / 2.4,
                                      child: ListView.builder(
                                        scrollDirection: Axis.vertical,
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: news.length,
                                        itemBuilder: (context, index) {
                                          if (index % 2 == 0)
                                            return NewsCard(
                                              data: news[index],
                                              refresh: getData,
                                            );
                                          else {
                                            return Container();
                                          }
                                        },
                                      ),
                                    ),
                                    Container(
                                      width: size.width / 2.4,
                                      child: ListView.builder(
                                        scrollDirection: Axis.vertical,
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: news.length,
                                        itemBuilder: (context, index) {
                                          if (index % 2 != 0)
                                            return NewsCard(
                                              data: news[index],
                                              refresh: getData,
                                            );
                                          else {
                                            return Container();
                                          }
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 100,
                              )
                            ],
                          ),
                        ),
                      ),
          ],
        ),
      ),
    );
  }
}
