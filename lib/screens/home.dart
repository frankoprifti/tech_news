import 'package:flutter/material.dart';
import 'package:tech_news/api/request.dart';
import 'package:tech_news/widgets/carousel.dart';
import 'package:tech_news/widgets/news_card.dart';
import 'package:lottie/lottie.dart';
import 'package:soft_edge_blur/soft_edge_blur.dart'; // Import soft_edge_blur package

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var smallLoader = false;
  var data = [];
  var carousel = [];
  var news = [];
  var page = 1;
  var loading = true;

  void getData() async {
    var response = await request("/posts?per_page=12&page=$page", false);
    if (response['success'] != null) {
      var carouselData = [];
      var newsData = [];
      for (var i = 0; i <= 3; i++) {
        carouselData.add(response['success'][i]);
      }
      for (var i = 4; i < response['success'].length; i++) {
        newsData.add(response['success'][i]);
      }
      if (mounted) {
        setState(() {
          carousel = carouselData;
          data = response['success'];
          news = newsData;
          loading = false;
        });
      }
    } else {
      if (mounted) {
        setState(() {
          loading = false;
        });
      }
    }
  }

  void getMoreData() async {
    var response = await request("/posts?per_page=12&page=$page", false);
    if (response['success'] != null) {
      for (var i = 0; i < response['success'].length; i++) {
        news.add(response['success'][i]);
      }
      if (mounted) {
        setState(() {
          loading = false;
          smallLoader = false;
        });
      }
    } else {
      if (mounted) {
        setState(() {
          smallLoader = false;
        });
      }
    }
  }

  ScrollController scrollController = new ScrollController();

  @override
  void initState() {
    scrollController.addListener(() {
      var pos = scrollController.position;
      if (pos.pixels == pos.maxScrollExtent) {
        setState(() {
          smallLoader = true;
          page = page + 1;
        });
        getMoreData();
      }
    });
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      body: SoftEdgeBlur(
        edges: [
          EdgeBlur(
            type: EdgeType.topEdge,
            size: 180,
            sigma: 280,
            tileMode: TileMode.mirror,
            controlPoints: [
              ControlPoint(
                position: 0.5,
                type: ControlPointType.visible,
              ),
              ControlPoint(
                position: 1,
                type: ControlPointType.transparent,
              )
            ],
          ),
        ],
        child: loading
            ? Container(
                color: Color(0xff444444),
                width: size.width,
                height: size.height,
                child: Center(
                  child: Container(
                      width: 150,
                      height: 150,
                      child: Lottie.asset('lib/assets/lottie/loading.json')),
                ),
              )
            : (!loading && news.length == 0)
                ? Container(
                    color: Color(0xff444444),
                    width: size.width,
                    height: size.height,
                    child: Center(
                      child: Container(
                        height: 250,
                        child: Column(
                          children: [
                            Container(
                                width: 250,
                                height: 250,
                                child: Lottie.asset(
                                    'lib/assets/lottie/network_error.json')),
                          ],
                        ),
                      ),
                    ),
                  )
                : Material(
                    color: Colors.transparent,
                    child: Container(
                      width: size.width,
                      height: size.height,
                      decoration: BoxDecoration(color: Color(0xff444444)),
                      child: RefreshIndicator(
                        color: Colors.white,
                        backgroundColor: Color(0xff444444),
                        onRefresh: () async {
                          setState(() {
                            loading = true;
                            page = 1;
                          });
                          return getData();
                        },
                        child: SingleChildScrollView(
                          controller: scrollController,
                          child: Column(
                            children: [
                              MyCarousel(carousel),
                              Row(
                                children: [
                                  SizedBox(
                                    width: 24,
                                  ),
                                  Text(
                                    'More News',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        decoration: TextDecoration.none),
                                  ),
                                ],
                              ),
                              Transform(
                                transform:
                                    Matrix4.translationValues(0.0, -35.0, 0.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Container(
                                      width: size.width / 2.4,
                                      child: ListView.builder(
                                        scrollDirection: Axis.vertical,
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: news.length,
                                        itemBuilder: (context, index) {
                                          if (index % 2 == 0)
                                            return NewsCard(data: news[index]);
                                          else {
                                            return Container();
                                          }
                                        },
                                      ),
                                    ),
                                    Container(
                                      width: size.width / 2.4,
                                      child: ListView.builder(
                                        scrollDirection: Axis.vertical,
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: news.length,
                                        itemBuilder: (context, index) {
                                          if (index % 2 != 0)
                                            return NewsCard(data: news[index]);
                                          else {
                                            return Container();
                                          }
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              smallLoader
                                  ? Container(
                                      width: 80,
                                      height: 80,
                                      child: Lottie.asset(
                                          'lib/assets/lottie/loading.json'))
                                  : Container(),
                              SizedBox(
                                height: 100,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
      ),
    );
  }
}
