import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tech_news/api/request.dart';
import 'package:tech_news/widgets/custom_app_bar.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:shimmer/shimmer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:transparent_image/transparent_image.dart';

class News extends StatefulWidget {
  final data;
  News(this.data);
  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<News> {
  var categories = [];
  var loading = true;
  var stateIsSaved = false;
  void getData() async {
    var response =
        await request(widget.data["_links"]["wp:term"][3]["href"], true);

    if (response['success'] != null) {
      var data = response['success'] ?? [];
      var categoryNames = [];
      for (var i = 0; i < data.length; i++) {
        if (i <= 2) {
          categoryNames.add(data[i]["name"]);
        }
      }
      if (mounted) {
        setState(() {
          categories = categoryNames;
          loading = false;
        });
      }
    } else {
      // Flushbar(
      //   mainButton: FlatButton(
      //     onPressed: () {
      //       getData();
      //     },
      //     child: Text(
      //       "Try Again",
      //       style: TextStyle(color: Colors.red[300]),
      //     ),
      //   ),
      //   flushbarPosition: FlushbarPosition.TOP,
      //   leftBarIndicatorColor: Colors.red[300],
      //   margin: EdgeInsets.only(top: 18, left: 8, right: 8),
      //   borderRadius: 8,
      //   message: "Error Getting Cattegories",
      //   duration: null,
      // )..show(context);
      
      if (mounted) {
        setState(() {
          loading = false;
        });
      }
    }
  }

  void saveNews() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var list = prefs.getStringList('news') ?? [];
    list.add(json.encode(widget.data));
    await prefs.setStringList('news', list);
    setState(() {
      stateIsSaved = true;
    });
    // Flushbar(
    //   mainButton: FlatButton(
    //     onPressed: () {
    //       unsaveNews();
    //     },
    //     child: Text(
    //       "UNSAVE",
    //       style: TextStyle(color: Colors.white),
    //     ),
    //   ),
    //   flushbarPosition: FlushbarPosition.BOTTOM,
    //   leftBarIndicatorColor: Colors.white,
    //   margin: EdgeInsets.only(bottom: 18, left: 8, right: 8),
    //   borderRadius: 8,
    //   message: "${widget.data['title']['rendered']} is now saved!",
    //   duration: Duration(seconds: 3),
    // )..show(context);
  
  }

  void unsaveNews() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var list = prefs.getStringList('news') ?? [];
    for (var i = 0; i < list.length; i++) {
      if (json.decode(list[i])['id'] == widget.data['id']) {
        list.removeAt(i);
      }
    }
    await prefs.setStringList('news', list);
    setState(() {
      stateIsSaved = false;
    });
    // Flushbar(
    //   mainButton: FlatButton(
    //     onPressed: () {
    //       saveNews();
    //     },
    //     child: Text(
    //       "SAVE",
    //       style: TextStyle(color: Colors.white),
    //     ),
    //   ),
    //   flushbarPosition: FlushbarPosition.BOTTOM,
    //   leftBarIndicatorColor: Colors.white,
    //   margin: EdgeInsets.only(bottom: 18, left: 8, right: 8),
    //   borderRadius: 8,
    //   message: "${widget.data['title']['rendered']} is now removed from saved!",
    //   duration: Duration(seconds: 3),
    // )..show(context);
  
  }

  void getIfSaved() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var list = prefs.getStringList('news') ?? [];
    var isSaved = false;
    for (var i = 0; i < list.length; i++) {
      if (json.decode(list[i])['id'] == widget.data['id']) {
        isSaved = true;
      }
    }
    setState(() {
      stateIsSaved = isSaved;
    });
  }

  @override
  void initState() {
    getIfSaved();
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Material(
      color: Colors.transparent,
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            color: Color(0xff444444),
            height: size.height,
            width: size.width,
            child: SingleChildScrollView(
                child: Column(
              children: [
                SizedBox(height: 135),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Colors.greenAccent,
                        onTap: () {
                          FlutterWebBrowser.openWebPage(
                            url: widget.data['jetpack_featured_media_url'],
                            safariVCOptions: SafariViewControllerOptions(
                              barCollapsingEnabled: true,
                              preferredBarTintColor: Colors.green,
                              preferredControlTintColor: Colors.amber,
                              dismissButtonStyle:
                                  SafariViewControllerDismissButtonStyle.close,
                              modalPresentationCapturesStatusBarAppearance:
                                  true,
                            ),
                          );
                        },
                        child: FadeInImage.memoryNetwork(
                          fadeInCurve: Curves.bounceInOut,
                          fadeInDuration: Duration(milliseconds: 150),
                          fadeOutDuration: Duration(milliseconds: 150),
                          placeholder: kTransparentImage,
                          image: widget.data['jetpack_featured_media_url'],
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                loading
                    ? Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 19.0, vertical: 8),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: Shimmer.fromColors(
                                  baseColor: Colors.grey[600]!,
                                  highlightColor: Colors.grey[400]!,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(6))),
                                    width: 70,
                                    height: 20,
                                  )),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: Shimmer.fromColors(
                                  baseColor: Colors.grey[600]!,
                                  highlightColor: Colors.grey[400]!,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(6))),
                                    width: 70,
                                    height: 20,
                                  )),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: Shimmer.fromColors(
                                  baseColor: Colors.grey[600]!,
                                  highlightColor: Colors.grey[400]!,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(6))),
                                    width: 70,
                                    height: 20,
                                  )),
                            ),
                          ],
                        ),
                      )
                    : Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 19.0, vertical: 8),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Container(
                            width: size.width,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: List.generate(
                                categories.length,
                                (index) => Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.white, width: 1),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(6))),
                                    child: Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Text(
                                        categories[index],
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19.0),
                  child: Row(
                    children: [
                      Container(
                        width: size.width - 45,
                        child: Text(
                          widget.data['title']['rendered'],
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Html(
                    data: widget.data['content']['rendered'],
                    // onLinkTap: (url) {
                    //   FlutterWebBrowser.openWebPage(
                    //     url: url,
                    //     androidToolbarColor: Color(0xff444444),
                    //     safariVCOptions: SafariViewControllerOptions(
                    //       barCollapsingEnabled: true,
                    //       preferredBarTintColor: Colors.green,
                    //       preferredControlTintColor: Colors.amber,
                    //       dismissButtonStyle:
                    //           SafariViewControllerDismissButtonStyle.close,
                    //       modalPresentationCapturesStatusBarAppearance: true,
                    //     ),
                    //   );
                    // },
                    // onImageTap: (url) {
                    //   FlutterWebBrowser.openWebPage(
                    //     url: url,
                    //     androidToolbarColor: Color(0xff444444),
                    //     safariVCOptions: SafariViewControllerOptions(
                    //       barCollapsingEnabled: true,
                    //       preferredBarTintColor: Colors.green,
                    //       preferredControlTintColor: Colors.amber,
                    //       dismissButtonStyle:
                    //           SafariViewControllerDismissButtonStyle.close,
                    //       modalPresentationCapturesStatusBarAppearance: true,
                    //     ),
                    //   );
                    // },
                    
                    style: {
                      "video": Style(height: Height(220)),
                      "ul": Style(
                        listStyleType: ListStyleType.disc,
                      ),
                      "p": Style(color: Colors.white),
                      "h1": Style(color: Colors.white),
                      "h2": Style(color: Colors.white),
                      "h3": Style(color: Colors.white),
                      "h4": Style(color: Colors.white),
                      "h5": Style(color: Colors.white),
                      "h6": Style(color: Colors.white),
                      "a": Style(
                        color: Colors.lightBlueAccent,
                      ),
                      "b": Style(
                          color: Colors.white, fontWeight: FontWeight.bold),
                      "span": Style(color: Colors.white),
                      "li": Style(color: Colors.white),
                      "hr": Style(
                        color: Colors.white,
                        border: Border(bottom: BorderSide(color: Colors.white)),
                      ),
                    },
                  ),
                ),
              ],
            )),
          ),
          MyAppBar(widget.data, saveNews, stateIsSaved, unsaveNews),
        ],
      ),
    );
  }
}
