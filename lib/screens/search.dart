
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:tech_news/api/request.dart';
import 'package:tech_news/widgets/news_card.dart';
import 'package:tech_news/widgets/search_box.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  var news = [];
  var loading = false;
  String searchText = '';
  void getSearchText(txt) {
    if (txt != null && txt != '' && txt != " " && txt.toString().length >= 4) {
      setState(() {
        searchText = txt;
      });
      getData(txt);
    }
  }

  void getData(txt) async {
    var newsWeWant = [];
    if (txt != null && txt != '' && txt != " " && txt.toString().length >= 4) {
      setState(() {
        loading = true;
      });
      var response =
          await request("/search?per_page=12&search=$txt&type=post", false);
      if (response['success'] != null) {
        if (mounted) {
          var result = response['success'];

          for (var i = 0; i < result.length; i++) {
            var miniResp =
                await request(result[i]["_links"]["self"][0]["href"], true);
            if (miniResp['success'] != null) {
              newsWeWant.add(miniResp['success']);
            }
          }
          if (newsWeWant.length == response['success'].length) {
            setState(() {
              news = newsWeWant;
              loading = false;
            });
          }
        }
      } else {
        // Flushbar(
        //   mainButton: FlatButton(
        //     onPressed: () {
        //       getData(searchText);
        //     },
        //     child: Text(
        //       "Try Again",
        //       style: TextStyle(color: Colors.red[300]),
        //     ),
        //   ),
        //   flushbarPosition: FlushbarPosition.TOP,
        //   leftBarIndicatorColor: Colors.red[300],
        //   margin: EdgeInsets.only(top: 18, left: 8, right: 8),
        //   borderRadius: 8,
        //   message: "Error Getting News",
        //   duration: null,
        // )..show(context);
        
        if (mounted) {
          setState(() {
            loading = false;
          });
        }
      }
    } else {
      newsWeWant = [];
      setState(() {
        news = newsWeWant;
      });
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Material(
      color: Colors.transparent,
      child: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(color: Color(0xff444444)),
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            loading
                ? Container(
                    color: Color(0xff444444),
                    width: size.width,
                    height: size.height,
                    child: Center(
                      child: Container(
                          width: 150,
                          height: 150,
                          child:
                              Lottie.asset('lib/assets/lottie/loading.json')),
                    ),
                  )
                : SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 105,
                        ),
                        searchText == ''
                            ? Container()
                            : Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 28.0),
                                    child: Text(
                                      'Search Results For "$searchText"',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          decoration: TextDecoration.none),
                                    ),
                                  ),
                                ],
                              ),
                        Transform(
                          transform: Matrix4.translationValues(0.0, -35.0, 0.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Container(
                                width: size.width / 2.4,
                                child: ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: news.length,
                                  itemBuilder: (context, index) {
                                    if (index % 2 == 0)
                                      return NewsCard(
                                        data: news[index],
                                      );
                                    else {
                                      return Container();
                                    }
                                  },
                                ),
                              ),
                              Container(
                                width: size.width / 2.4,
                                child: ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: news.length,
                                  itemBuilder: (context, index) {
                                    if (index % 2 != 0)
                                      return NewsCard(data: news[index]);
                                    else {
                                      return Container();
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 100,
                        )
                      ],
                    ),
                  ),
            SearchBox(getSearchText),
          ],
        ),
      ),
    );
  }
}
