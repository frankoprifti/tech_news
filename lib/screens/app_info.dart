import 'package:flutter/material.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:package_info/package_info.dart';

class AppInfo extends StatefulWidget {
  @override
  _AppInfoState createState() => _AppInfoState();
}

class _AppInfoState extends State<AppInfo> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );
  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  void initState() {
    _initPackageInfo();
    super.initState();
  }

  Widget _infoTile(String title, String subtitle) {
    return ListTile(
      title: Text(
        title,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      subtitle: Text(subtitle ?? 'Not set',
          style: TextStyle(
            color: Colors.white,
          )),
    );
  }

  void launch(url) {
    FlutterWebBrowser.openWebPage(
      url: url,
      safariVCOptions: SafariViewControllerOptions(
        barCollapsingEnabled: true,
        preferredBarTintColor: Colors.green,
        preferredControlTintColor: Colors.amber,
        dismissButtonStyle: SafariViewControllerDismissButtonStyle.close,
        modalPresentationCapturesStatusBarAppearance: true,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Material(
      color: Colors.transparent,
      child: Container(
        width: size.width,
        height: size.height,
        color: Color(0xff444444),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 70,
              ),
              Image.asset('lib/assets/icons/app_icon.png'),
              Material(
                color: Colors.transparent,
                child: InkWell(
                  splashColor: Colors.green[50],
                  onTap: () {
                    launch('https://9to5google.com');
                  },
                  child: Image.asset('lib/assets/icons/9to5-google-logo.png',
                      width: size.width / 2),
                ),
              ),
              _infoTile('App name', _packageInfo.appName),
              _infoTile('Package name', _packageInfo.packageName),
              _infoTile('App version', _packageInfo.version),
              _infoTile('Build number', _packageInfo.buildNumber),
              _infoTile('Disclaimer',
                  'Tech News started as a test for a beautiful UI application. Posts/news that you see on this app are fetched from "https://9to5google.com/" and I never intended to steal from them. I have contacted 9to5google about this app and also I have no intention to earn money from this app. If someone or 9to5google itself does not agree for using their posts, please contact me first on frankoprifti@gmail.com and I will halt immediately.'),
              Material(
                color: Colors.transparent,
                child: InkWell(
                  splashColor: Colors.green[50],
                  onTap: () {
                    launch('mailto:frankoprifti@gmail.com');
                  },
                  child: Container(
                    child: ListTile(
                      title: Text(
                        'Contact Dev (Click Me)',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text('frankoprifti@gmail.com' ?? 'Not set',
                          style: TextStyle(
                            color: Colors.white,
                          )),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 120,
              )
            ],
          ),
        ),
      ),
    );
  }
}
