import 'package:http/http.dart' as http;
import 'dart:convert';

var url = 'https://9to5google.com/wp-json/wp/v2';
Future request(route, full) async {
  if (full) {
    var response = await http.get(route);
    if (response.statusCode == 200) {
      return {"success": json.decode(response.body)};
    } else {
      return {'error': 'Error Getting News'};
    }
  } else {
    var response = await http.get(Uri.parse(url + route));
    if (response.statusCode == 200) {
      return {"success": json.decode(response.body)};
    } else {
      return {'error': 'Error Getting News'};
    }
  }
}
