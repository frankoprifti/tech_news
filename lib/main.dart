import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:tech_news/screens/app_info.dart';
import 'package:tech_news/screens/home.dart';
import 'package:tech_news/screens/saved.dart';
import 'package:tech_news/screens/search.dart';
import 'package:tech_news/widgets/nav_bar.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var active = 0;
  void changeActive(nr) {
    setState(() {
      active = nr;
    });
  }

  var network = true;

  @override
  void initState() {
    checkNetwork();
    super.initState();
  }

  void checkNetwork() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() {
          network = true;
        });
      }
    } on SocketException catch (_) {
      setState(() {
        network = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));

    return MaterialApp(
      title: 'Tech News',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: network
          ? Stack(
              alignment: Alignment.bottomCenter,
              children: [
                active == 0
                    ? Home()
                    : active == 1
                        ? Search()
                        : active == 2 ? Saved() : AppInfo(),
                NavBar(active, changeActive)
              ],
            )
          : Material(
              color: Colors.transparent,
              child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: Color(0xff444444),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Lottie.asset('lib/assets/lottie/error.json'),
                      Text(
                        'No internet connection',
                        style: TextStyle(
                            color: Colors.white,
                            decoration: TextDecoration.none,
                            fontSize: 16),
                      ),
                      // FlatButton(
                      //   onPressed: checkNetwork,
                      //   child: Text(
                      //     'Retry',
                      //     style: TextStyle(
                      //         color: Colors.white,
                      //         decoration: TextDecoration.none,
                      //         fontSize: 16),
                      //   ),
                      //   color: Colors.red,
                      // )
                    
                    ],
                  ))),
            ),
    );
  }
}
