import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:tech_news/screens/news.dart';
import 'package:transparent_image/transparent_image.dart';

class MyCarousel extends StatefulWidget {
  var data;
  MyCarousel(this.data);
  @override
  _MyCarouselState createState() => _MyCarouselState();
}

class _MyCarouselState extends State<MyCarousel> {
  var items = [];
  var page = 0;
  double height = 0;
  @override
  void initState() {
    super.initState();
    items = widget.data;
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    void animateAndNavigate(i) async {
      setState(() {
        height = height + 5;
      });
      await Future.delayed(Duration(milliseconds: 70));
      setState(() {
        height = height - 5;
      });
      await Future.delayed(Duration(milliseconds: 70));
      Navigator.push(
        context,
        MaterialPageRoute(maintainState: true, builder: (context) => News(i)),
      );
    }

    return Stack(
      alignment: Alignment.topCenter,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          child: Transform.scale(
            scale: 1.3,
            child: Container(
              child: Stack(alignment: Alignment.center, children: [
                FadeInImage.memoryNetwork(
                  fadeInCurve: Curves.bounceInOut,
                  fadeInDuration: Duration(milliseconds: 150),
                  fadeOutDuration: Duration(milliseconds: 150),
                  placeholder: kTransparentImage,
                  image: widget.data[page]['jetpack_featured_media_url'],
                  fit: BoxFit.cover,
                ),
                BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 12, sigmaY: 12),
                  child: Container(
                    color: Color(0xff373737).withAlpha(90),
                  ),
                ),
              ]),
              decoration: BoxDecoration(),
              width: size.width,
              height: size.height / 3.5,
            ),
          ),
        ),
        Container(
            child: Column(
          children: [
            SizedBox(
              height: 40,
            ),
            Material(
              color: Colors.transparent,
              child: Row(
                children: [
                  SizedBox(
                    width: 24,
                  ),
                  Text(
                    'Latest News',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        decoration: TextDecoration.none),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 8,
            ),
            CarouselSlider(
              options: CarouselOptions(
                initialPage: 0,
                onPageChanged: (index, reason) {
                  setState(() {
                    page = index;
                  });
                },
                autoPlayInterval: Duration(seconds: 15),
                viewportFraction: 1.0,
                height: 230.0,
                autoPlay: true,
              ),
              items: items.map((i) {
                return Builder(
                  builder: (BuildContext context) {
                    return ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      child: AnimatedContainer(
                          transform:
                              Matrix4.translationValues(0.0, height, 0.0),
                          duration: Duration(milliseconds: 70),
                          curve: Curves.easeIn,
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.symmetric(horizontal: 25.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                          ),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              splashColor: Colors.black.withAlpha(50),
                              onTap: () {
                                animateAndNavigate(i);
                              },
                              child: Stack(
                                alignment: Alignment.bottomCenter,
                                children: [
                                  ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8)),
                                    child: FadeInImage.memoryNetwork(
                                      fadeInCurve: Curves.bounceInOut,
                                      fadeInDuration:
                                          Duration(milliseconds: 150),
                                      fadeOutDuration:
                                          Duration(milliseconds: 150),
                                      placeholder: kTransparentImage,
                                      image: i['jetpack_featured_media_url'],
                                      height: 230,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Material(
                                    color: Colors.transparent,
                                    child: Stack(
                                      alignment: Alignment.bottomCenter,
                                      children: [
                                        ClipRRect(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8)),
                                          child: Wrap(
                                            children: [
                                              Container(
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: Text(
                                                        "${i['title']['rendered'].toString().replaceAll(new RegExp(r'&#8217;'), '\'')}",
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 2,
                                                        softWrap: true,
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            decoration:
                                                                TextDecoration
                                                                    .none,
                                                            fontSize: 16),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                decoration: BoxDecoration(
                                                    color: Color(0xff7b7b7b)
                                                        .withOpacity(0.80)),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )),
                    );
                  },
                );
              }).toList(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: items.map((url) {
                int index = items.indexOf(url);
                return Container(
                  width: 8.0,
                  height: 8.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color:
                        page == index ? Color(0xffc4c4c4) : Color(0xff757575),
                  ),
                );
              }).toList(),
            ),
          ],
        )),
      ],
    );
  }
}
