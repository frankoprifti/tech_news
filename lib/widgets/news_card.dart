
import 'package:flutter/material.dart';
import 'package:tech_news/screens/news.dart';
import 'package:transparent_image/transparent_image.dart';

class NewsCard extends StatefulWidget {
  final data;
  final refresh;
  NewsCard({@required this.data, this.refresh});
  @override
  _NewsCardState createState() => _NewsCardState();
}

class _NewsCardState extends State<NewsCard> {
  double height = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    void animateAndNavigate() async {
      setState(() {
        height = height - 10;
      });
      await Future.delayed(Duration(milliseconds: 70));
      setState(() {
        height = height + 10;
      });
      await Future.delayed(Duration(milliseconds: 70));
      Navigator.push(
        context,
        MaterialPageRoute(
            maintainState: true, builder: (context) => News(widget.data)),
      ).then((value) {
        if (widget.refresh != null) {
          widget.refresh();
        }
      });
    }

    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(8)),
      child: AnimatedContainer(
        transform: Matrix4.translationValues(0.0, height, 0.0),
        curve: Curves.easeIn,
        duration: Duration(milliseconds: 70),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        margin: EdgeInsets.symmetric(vertical: 15),
        width: size.width / 2.4,
        height: 250,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Material(
              color: Colors.transparent,
              child: GestureDetector(
                onTap: () {
                  animateAndNavigate();
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  child: FadeInImage.memoryNetwork(
                    fadeInCurve: Curves.bounceInOut,
                    fadeInDuration: Duration(milliseconds: 150),
                    fadeOutDuration: Duration(milliseconds: 150),
                    placeholder: kTransparentImage,
                    width: size.width / 2.4,
                    height: 240,
                    image: widget.data['jetpack_featured_media_url'],
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Material(
              color: Colors.transparent,
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    child: Wrap(
                      children: [
                        Container(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "${widget.data['title']['rendered'].toString().replaceAll(new RegExp(r'&#8217;'), '\'')}",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                  softWrap: true,
                                  style: TextStyle(
                                      color: Colors.white,
                                      decoration: TextDecoration.none,
                                      fontSize: 12),
                                ),
                              ),
                            ],
                          ),
                          decoration: BoxDecoration(
                              color: Color(0xff7b7b7b).withOpacity(0.80)),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
