import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class NavIcon extends StatelessWidget {
  String name;
  String link;
  bool active;
  NavIcon(this.name, this.link, this.active);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(this.link),
          SizedBox(
            height: 8,
          ),
          AnimatedDefaultTextStyle(
            duration: Duration(milliseconds: 150),
            child: Text(this.name),
            style: this.active
                ? TextStyle(color: Colors.white, fontSize: 14)
                : TextStyle(color: Color(0xffC5C5C5), fontSize: 12),
          ),
        ],
      ),
    );
  }
}
