import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SearchBox extends StatefulWidget {
  var changeTxt;
  SearchBox(this.changeTxt);
  @override
  _SearchBoxState createState() => _SearchBoxState();
}

class _SearchBoxState extends State<SearchBox> {
  String search='';
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        margin: EdgeInsets.only(top: 45),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 12, sigmaY: 12),
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xff666666).withOpacity(0.60),
                ),
                child: TextField(
                  onChanged: (txt) {
                    setState(() {
                      search = txt;
                    });
                    widget.changeTxt(txt);
                  },
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                      icon: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Container(
                          width: 24,
                          child: SvgPicture.asset(
                            "lib/assets/icons/search_inactive.svg",
                            width: 20,
                          ),
                        ),
                      ),
                      contentPadding: EdgeInsets.all(8),
                      border: InputBorder.none,
                      hintStyle: TextStyle(color: Color(0xffB2B2B2)),
                      hintText: "Search"),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
