import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tech_news/widgets/nav_icon.dart';

class NavBar extends StatefulWidget {
  final active;
  final changeActive;
  NavBar(this.active, this.changeActive);
  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(18), topRight: Radius.circular(18)),
      child: ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 12, sigmaY: 12),
          child: Container(
            height: 80,
            decoration:
                BoxDecoration(color: Color(0xff666666).withOpacity(0.60)),
            child: Material(
              color: Colors.transparent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  InkWell(
                    customBorder: CircleBorder(),
                    onTap: () {
                      widget.changeActive(0);
                    },
                    child: NavIcon(
                        'Home',
                        widget.active == 0
                            ? 'lib/assets/icons/home_active.svg'
                            : 'lib/assets/icons/home_inactive.svg',
                        widget.active == 0 ? true : false),
                  ),
                  InkWell(
                    customBorder: CircleBorder(),
                    onTap: () {
                      widget.changeActive(1);
                    },
                    child: NavIcon(
                        'Search',
                        widget.active == 1
                            ? 'lib/assets/icons/search_active.svg'
                            : 'lib/assets/icons/search_inactive.svg',
                        widget.active == 1 ? true : false),
                  ),
                  InkWell(
                    customBorder: CircleBorder(),
                    onTap: () {
                      widget.changeActive(2);
                    },
                    child: NavIcon(
                        'Saved',
                        widget.active == 2
                            ? 'lib/assets/icons/save_active.svg'
                            : 'lib/assets/icons/save_inactive.svg',
                        widget.active == 2 ? true : false),
                  ),
                  InkWell(
                    customBorder: CircleBorder(),
                    onTap: () {
                      widget.changeActive(3);
                    },
                    child: NavIcon(
                        'App Info',
                        widget.active == 3
                            ? 'lib/assets/icons/settings_active.svg'
                            : 'lib/assets/icons/settings_inactive.svg',
                        widget.active == 3 ? true : false),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
